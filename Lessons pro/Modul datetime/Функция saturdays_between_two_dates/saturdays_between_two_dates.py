from datetime import date

""" 3.1 Функция saturdays_between_two_dates()
Реализуйте функцию saturdays_between_two_dates(), которая принимает два аргумента в следующем порядке:
start — начальная дата, тип date
end — конечная дата, тип date
Функция должна возвращать количество суббот между датами start и end включительно.
Примечание 1. Даты могут передаваться в любом порядке, то есть не гарантируется, что первая дата меньше второй.
Примечание 2. В тестирующую систему сдайте программу,
содержащую только необходимую функцию saturdays_between_two_dates(), но не код, вызывающий ее.
"""


def saturdays_between_two_dates(start, end):
    if start < end:
        x = [date.fromordinal(i).weekday() for i in range(start.toordinal(), end.toordinal() + 1)]
        return int(sum([i for i in x if i == 5]) / 5)
    else:
        start, end = end, start
        x = [date.fromordinal(i).weekday() for i in range(start.toordinal(), end.toordinal() + 1)]
        return int(sum([i for i in x if i == 5]) / 5)


# other
def saturdays_between_two_dates(start, end):
    if start > end:
        start, end = end, start

    return sum(date.fromordinal(i).weekday() == 5 for i in range(start.toordinal(), end.toordinal() + 1))


# # other code
# def saturdays_between_two_dates(start, end):
#     start, end = sorted([start, end])
#     return sum([date.fromordinal(i).isoweekday() == 6 for i in range(start.toordinal(), end.toordinal() + 1)])


date1 = date(2021, 11, 1)
date2 = date(2021, 11, 22)

print(saturdays_between_two_dates(date1, date2))

date1 = date(2020, 7, 26)
date2 = date(2020, 7, 2)

print(saturdays_between_two_dates(date1, date2))
