from datetime import datetime, date

""" 3.4 Функция num_of_sundays()
Реализуйте функцию num_of_sundays(), которая принимает на вход один аргумент:
year — натуральное число, год
Функция должна возвращать количество воскресений в году year.
Примечание 1. В тестирующую систему сдайте программу, содержащую только необходимую функцию num_of_sundays(), 
но не код, вызывающий ее.
"""


def num_of_sundays(year):
    start = date(year, 1, 1)
    end = date(year, 12, 31)
    return sum(date.fromordinal(i).weekday() == 6 for i in range(start.toordinal(), end.toordinal() + 1))


# # other code
# from datetime import date
#
# def num_of_sundays(year):
#     return date(year, 12, 31).strftime('%U')
#
year = 2000
print(num_of_sundays(year))

print(num_of_sundays(768))

print(num_of_sundays(2021))
