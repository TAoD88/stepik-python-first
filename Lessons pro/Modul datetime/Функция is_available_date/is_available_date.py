from datetime import datetime, date

""" 3.3 Функция is_available_date() 🌶️
Во время визита очередного гостя сотрудникам отеля приходится проверять, доступна ли та или иная дата для бронирования номера.
Реализуйте функцию is_available_date(), которая принимает два аргумента в следующем порядке:
booked_dates — список строковых дат, недоступных для бронирования. Элементом списка является либо одиночная дата, 
либо период (две даты через дефис). Например:
['04.11.2021', '05.11.2021-09.11.2021']
date_for_booking — одиночная строковая дата или период (две даты через дефис), на которую гость желает забронировать номер. 
Например:
'01.11.2021' или '01.11.2021-04.11.2021'
Функция is_available_date() должна возвращать True, если дата или период date_for_booking полностью доступна для бронирования. В противном случае функция должна возвращать False.
Примечание 1. Гарантируется, что в периоде левая дата всегда меньше правой.
Примечание 2. В периоде (две даты через дефис) граничные даты включены.
Примечание 3. В тестирующую систему сдайте программу, содержащую только необходимую функцию is_available_date(), 
но не код, вызывающий ее.
"""


def some_date_test(some_date):
    list_date = []
    if len(some_date) > 10:
        some_a, some_b = some_date.split('-')
        some_a = some_a.split('.')
        some_b = some_b.split('.')
        day_a = date(int(some_a[2]), int(some_a[1]), int(some_a[0])).toordinal()
        day_b = date(int(some_b[2]), int(some_b[1]), int(some_b[0])).toordinal()
        list_date = [day for day in range(int(day_a), int(day_b) + 1)]
        return list_date
    else:
        data, month, yaer = some_date.split('.')
        day = date(int(yaer), int(month), int(data)).toordinal()
        list_date.append(int(day))
        return list_date


def dates_test(dates):
    list_date_res = []
    for each in dates:
        list_date_res.append(some_date_test(each))
    return list_date_res


def is_available_date(dates, some_date) -> bool:
    flag = True
    for each in dates_test(dates):
        for x in each:
            if x not in some_date_test(some_date):
                flag = flag
            else:
                flag = False
    return flag


# # Other code

# def str2period(str_dates):
#     period = tuple(map(lambda x: datetime.strptime(x, "%d.%m.%Y"), str_dates.split("-")))
#     return period if len(period) > 1 else period * 2
#
#
# def not_overlapping(period1, period2):
#     return period1[1] < period2[0] or period1[0] > period2[1]
#
#
# def is_available_date(booked_dates, date_for_booking):
#     check_dates = str2period(date_for_booking)
#     for bd in booked_dates:
#         if not not_overlapping(str2period(bd), check_dates):
#             return False
#     return True
#
#
# # Other code

# def sd(s): # функция превращает строку в дату
#     return datetime.strptime(s, '%d.%m.%Y')
# def dk(spd): # функция превращает строку c датой или интервалом дат в кортеж из 2 дат
#     return (sd(spd[:10]), sd(spd[11:])) if '-' in spd else (sd(spd), sd(spd))
# def is_available_date(sp, d):
#     for x in sp: # проверяем, пересекаются ли кортежи дат гостя и отеля
#         if not(dk(x)[1] < dk(d)[0] or dk(x)[0] > dk(d)[1]):
#             return False # если пересекаются, то выводим False
#     return True  # а если нет, то True
#
dates = ['04.11.2021', '05.11.2021-09.11.2021']
some_date = '01.11.2021'
print("TEST1:", is_available_date(dates, some_date))

dates = ['04.11.2021', '05.11.2021-09.11.2021']
some_date = '01.11.2021-04.11.2021'
print("TEST2:", is_available_date(dates, some_date))

dates = ['04.11.2021', '05.11.2021-09.11.2021']
some_date = '06.11.2021'
print("TEST3:", is_available_date(dates, some_date))

dates = ['01.11.2021', '05.11.2021-09.11.2021', '12.11.2021', '15.11.2021-21.11.2021']
some_date = '09.11.2021'
print("TEST4:", is_available_date(dates, some_date))
