from datetime import datetime, timedelta
""" 3.4 Реп по матеше
Репетитор по математике проводит занятия по 45 минут с перерывами по 10 минут. 
Репетитор обозначает время начала рабочего дня и время окончания рабочего дня. Напишите программу, 
которая генерирует и выводит расписание занятий.
Формат входных данных
На вход программе в первой строке подается время начала рабочего дня в формате HH:MM. 
В следующей строке вводится время окончания рабочего дня в том же формате.
Формат выходных данных
Программа должна сгенерировать и вывести расписание занятий. На первой строке выводится время начала и 
окончания первого занятия в формате HH:MM - HH:MM, на второй строке — время начала и окончания второго 
занятия в том же формате, и так далее.
Примечание 1. Если занятие обрывается временем окончания работы, то добавлять его в расписание не нужно.
Примечание 2. Если разница между временем начала и окончания рабочего дня меньше 45 минут, программа ничего не должна выводить.
"""



start_time =datetime.strptime(input(), '%H:%M')
finish_time =datetime.strptime(input(), '%H:%M')

while (start_time+timedelta(minutes=45))<=finish_time:
    print(start_time.strftime('%H:%M'),'-',(start_time+timedelta(minutes=45)).strftime('%H:%M'))
    start_time += timedelta(minutes=55)

# # Other code
#
# from datetime import datetime, timedelta
#
# f = '%H:%M'
# start = datetime.strptime(input(), f)
# end = datetime.strptime(input(), f)
# while True:
#     if start + timedelta(minutes=45) <= end:
#         temp = start + timedelta(minutes=45)
#         print(f'{start.strftime(f)} - {temp.strftime(f)}')
#         start = temp + timedelta(minutes=10)
#     else:
#         break
#
# # Other code
#
# from datetime import datetime, timedelta
# f = '%H:%M'
# start, stop = (datetime.strptime(input(), f) for i in '__')
# while start <= (stop - timedelta(minutes=45)):
#     print(start.strftime(f), '-', (start + timedelta(minutes=45)).strftime(f))
#     start += timedelta(minutes=55)
#
#
# # 10:00
# # 12:35