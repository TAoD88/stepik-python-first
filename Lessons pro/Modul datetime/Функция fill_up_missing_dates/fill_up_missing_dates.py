from datetime import datetime, timedelta

""" 3.4 Функция fill_up_missing_dates()
Реализуйте функцию fill_up_missing_dates(), которая принимает на вход один аргумент:
dates — список строковых дат в формате DD.MM.YYYY
Функция должна возвращать список, в котором содержатся все даты из списка dates, расположенные в порядке возрастания, 
а также все недостающие промежуточные даты.
Примечание 1. Рассмотрим первый тест. Список dates содержит период с 01.11.2021 по 07.11.2021:
dates = ['01.11.2021', '07.11.2021', '04.11.2021', '03.11.2021']
в котором отсутствуют даты 02.11.2021, 05.11.2021, 06.11.2021. Тогда вызов функции:
fill_up_missing_dates(dates)
должен вернуть список: 
['01.11.2021', '02.11.2021', '03.11.2021', '04.11.2021', '05.11.2021', '06.11.2021', '07.11.2021']
Примечание 2. Функция должна создавать и возвращать новый список, а не изменять переданный.
Примечание 3. В тестирующую систему сдайте программу, содержащую только необходимую функцию fill_up_missing_dates(), 
но не код, вызывающий ее.
"""


def fill_up_missing_dates(dates):
    dates = list(map(lambda x: datetime.strptime(x, '%d.%m.%Y'), dates))
    days = max(dates).toordinal() - min(dates).toordinal()
    rezult = [(min(dates) + timedelta(days=i)).strftime('%d.%m.%Y') for i in range(0, days + 1)]
    return rezult


# # Other code
#
# def fill_up_missing_dates(dates):
#     pattern = '%d.%m.%Y'
#     dates = [datetime.strptime(d, pattern) for d in dates]
#     start, end = min(dates), max(dates)
#     days = (end - start).days
#     return [(start + timedelta(days=i)).strftime(pattern) for i in range(days + 1)]
#
# # Other code
# from datetime import date, datetime
#
# def fill_up_missing_dates(dates : list[str]) -> list[date]:
#     dates = [datetime.strptime(day, "%d.%m.%Y").toordinal() for day in dates]
#     return [(date.fromordinal(day)).strftime("%d.%m.%Y") for day in range(min(dates), max(dates) + 1)]


dates = ['01.11.2021', '04.11.2021', '09.11.2021', '15.11.2021']
print(fill_up_missing_dates(dates))

dates = ['01.11.2021', '07.11.2021', '04.11.2021', '03.11.2021']
print(fill_up_missing_dates(dates))
