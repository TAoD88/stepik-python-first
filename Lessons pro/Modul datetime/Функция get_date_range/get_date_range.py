from datetime import date

""" 3.1 Функция get_date_range()
Реализуйте функцию get_date_range(), которая принимает два аргумента в следующем порядке:
start — начальная дата, тип date
end — конечная дата, тип date
Функция get_date_range() должна возвращать список, состоящий из дат (тип date) от start до end включительно.
Если start > end, функция должна вернуть пустой список.
Примечание 1. В тестирующую систему сдайте программу, содержащую только необходимую функцию get_date_range(),
но не код, вызывающий ее.
"""


def get_date_range(date1, date2):
    if date2.toordinal() > date1.toordinal():
        x = [date.fromordinal(i) for i in range(date1.toordinal(), date2.toordinal() + 1)]
        return x
    elif (date2.toordinal() - date1.toordinal()) == 0:
        x = [date.fromordinal(i) for i in range(date1.toordinal(), date2.toordinal() + 1)]
        return repr(x)
    else:
        return []


## other code
# def get_date_range(start, end):
#     return [date.fromordinal(i) for i in range(start.toordinal(), end.toordinal() + 1)]

date1 = date(2021, 10, 1)
date2 = date(2021, 10, 5)

print(*get_date_range(date1, date2), sep='\n')

date1 = date(2019, 6, 5)
date2 = date(2022, 6, 6)
print(len(get_date_range(date1, date2)))

date1 = date(2019, 6, 5)
date2 = date(2019, 6, 5)

print(get_date_range(date1, date2))

date1 = date(2025, 6, 5)
date2 = date(2022, 6, 6)
print(len(get_date_range(date1, date2)))
